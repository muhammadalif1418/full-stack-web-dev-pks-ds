// Soal 1 🔥

var daftarHewan = ['2. Komodo', '5. Buaya', '3. Cicak', '4. Ular', '1. Tokek']

daftarHewan.sort().forEach(function(butir) {
  console.log(butir)
  // 1. Tokek
  // 2. Komodo
  // 3. Cicak
  // 4. Ular
  // 5. Buaya
})

// Soal 2 🔥

function introduce(d) {
  return 'Nama saya ' + d.name + ', umur saya ' + d.age + ' tahun, alamat saya di ' + d.address + ', dan saya punya hobby, yaitu ' + d.hobby + '!'
}

var data = {name: 'John', age: 30, address: 'Jalan Pelesiran', hobby: 'gaming'}

var perkenalan = introduce(data)

console.log(perkenalan) // Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby, yaitu gaming!

// Soal 3 🔥

function hitung_huruf_vokal(s) {
  var huruf = s.toLowerCase().split('')

  var hurufVokal = huruf.filter(function(h) {
    return h == 'a' || h == 'e' || h == 'i' || h == 'o' || h == 'u'
  })

  return hurufVokal.length
}

var hitung_1 = hitung_huruf_vokal('Muhammad')
var hitung_2 = hitung_huruf_vokal('Iqbal')

console.log(hitung_1, hitung_2) // 3 2

// Soal 4 🔥

function hitung(i) {
  return i * 2 - 2
}

console.log(hitung(0)) // -2
console.log(hitung(1)) // 0
console.log(hitung(2)) // 2
console.log(hitung(3)) // 4
console.log(hitung(5)) // 8
