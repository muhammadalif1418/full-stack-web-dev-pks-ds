<?php
trait Hewan {
  public $nama;
  public $darah = 50;
  public $jumlahKaki;
  public $keahlian;

  public function atraksi() {
    echo $this->nama . ' sedang ' . $this->keahlian;
  }

  abstract public function getInfoHewan($jenisHewan);
}

trait Fight {
  public $attackPower;
  public $defencePower;

  public function diserang($penyerang) {
    echo $this->nama . ' sedang diserang';

    $this->darah -= $penyerang->attackPower / $this->defencePower;
  }

  public function serang($terserang) {
    echo $this->nama . ' sedang menyerang ' . $terserang->nama;
    echo '<br>';
    $terserang->diserang($this);
  }
}

class Elang {
  use Hewan;
  use Fight;

  public function __construct($nama) {
    $this->nama = $nama;
    $this->jumlahKaki = 2;
    $this->keahlian = 'terbang tinggi';
    $this->attackPower = 10;
    $this->defencePower = 5;
  }

  public function getInfoHewan($jenisHewan = 'elang') {
    echo 'Nama: ' . $this->nama;
    echo '<br>';
    echo 'Darah: ' . $this->darah;
    echo '<br>';
    echo 'Jumlah kaki: ' . $this->jumlahKaki;
    echo '<br>';
    echo 'Keahlian: ' . $this->keahlian;
    echo '<br>';
    echo 'Attack power: ' . $this->attackPower;
    echo '<br>';
    echo 'Defence power: ' . $this->defencePower;
    echo '<br>';
    echo 'Jenis hewan: ' . $jenisHewan;
  }
}

class Harimau {
  use Hewan;
  use Fight;

  public function __construct($nama) {
    $this->nama = $nama;
    $this->jumlahKaki = 4;
    $this->keahlian = 'lari cepat';
    $this->attackPower = 7;
    $this->defencePower = 8;
  }

  public function getInfoHewan($jenisHewan = 'harimau') {
    echo 'Nama: ' . $this->nama;
    echo '<br>';
    echo 'Darah: ' . $this->darah;
    echo '<br>';
    echo 'Jumlah kaki: ' . $this->jumlahKaki;
    echo '<br>';
    echo 'Keahlian: ' . $this->keahlian;
    echo '<br>';
    echo 'Attack power: ' . $this->attackPower;
    echo '<br>';
    echo 'Defence power: ' . $this->defencePower;
    echo '<br>';
    echo 'Jenis hewan: ' . $jenisHewan;
  }
}

$elang3 = new Elang('elang_3');
$harimau1 = new Harimau('harimau_1');
$harimau2 = new Harimau('harimau_2');

$elang3->getInfoHewan();
echo '<br><br>';
$harimau1->getInfoHewan();
echo '<br><br>';
$harimau2->getInfoHewan();
echo '<br><br>';

$harimau1->atraksi();
echo '<br><br>';
$elang3->atraksi();
echo '<br><br>';

$harimau1->serang($elang3);
echo '<br><br>';
$elang3->serang($harimau2);
echo '<br><br>';

$elang3->getInfoHewan();
echo '<br><br>';
$harimau1->getInfoHewan();
echo '<br><br>';
$harimau2->getInfoHewan();
