<?php

namespace App\Http\Controllers\Auth;

use App\Events\OtpCodeTeregenerasiEvent;
use App\Http\Controllers\Controller;
use App\OtpCode;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();

        if ($user->otpCode) {
            $user->otpCode->delete();
        }

        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $random)->first();
        } while ($check);

        $now = Carbon::now();

        $otpCode = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(5),
            'user_id' => $user->id,
        ]);

        // Kirim email OTP code ke email register
        event(new OtpCodeTeregenerasiEvent($otpCode));

        return response()->json([
            'success' => true,
            'message' => 'OTP Code berhasil diregenerasi',
            'data' => [
                'user' => $user,
                'otpCode' => $otpCode,
            ]
        ]);
    }
}
