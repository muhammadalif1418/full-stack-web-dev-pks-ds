<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'otp' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $otpCode = OtpCode::where('otp', $request->otp)->first();

        if (!$otpCode) {
            return response()->json([
                'success' => false,
                'message' => 'OTP Code tidak ditemukan',
            ], 404);
        }

        $now = Carbon::now();

        if ($now > $otpCode->valid_until) {
            return response()->json([
                'success' => false,
                'message' => 'OTP Code sudah kedaluwarsa',
            ], 400);
        }

        $user = $otpCode->user;

        $user->update([
            'email_verified_at' => $now,
        ]);

        $otpCode->delete();

        return response()->json([
            'success' => true,
            'message' => 'User berhasil diverifikasi',
            'data' => $user,
        ], 200);
    }
}
