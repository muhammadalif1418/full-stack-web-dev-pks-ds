<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserTerdaftarEvent;
use App\Http\Controllers\Controller;
use App\OtpCode;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'username' => [
                'required',
                // Regex untuk alpha_dash tanpa dash
                'regex:/^[A-Za-z0-9_]+$/',
                'max:30',
                'unique:users,username',
            ],
            'email' => 'required|email|unique:users,email',
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::create($allRequest);

        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $random)->first();
        } while ($check);

        $now = Carbon::now();

        $otpCode = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(5),
            'user_id' => $user->id,
        ]);

        // Kirim email OTP code ke email register
        event(new UserTerdaftarEvent($otpCode));

        return response()->json([
            'success' => true,
            'message' => 'Data User berhasil dibuat',
            'data' => [
                'user' => $user,
                'otpCode' => $otpCode,
            ]
        ]);
    }
}
