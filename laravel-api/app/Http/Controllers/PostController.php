<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function index()
    {
        // Ambil data dari table posts
        $posts = Post::latest()->get();

        // Buat respons JSON
        return response()->json([
            'success' => true,
            'message' => 'Lis data semua Post',
            'data' => $posts,
        ], 200);
    }

    public function show($id)
    {
        // Cari Post berdasarkan ID
        $post = Post::findOrFail($id);

        // Buat respons JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail sebuah Post',
            'data' => $post,
        ], 200);
    }

    public function store(Request $request)
    {
        // Atur validasi
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
        ]);

        // Respons validasi eror
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        // $user = auth()->user();

        // Simpan ke database
        $post = Post::create([
            'title' => $request->title,
            'description' => $request->description,
            // 'user_id' => $user->id,
        ]);

        // Sukses simpan ke database
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Post berhasil dibuat',
                'data' => $post,
            ], 201);
        }

        // Gagal simpan ke database
        return response()->json([
            'success' => false,
            'message' => 'Post gagal tersimpan',
        ], 409);
    }

    public function update(Request $request, $id)
    {
        // Atur validasi
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
        ]);

        // Respons validasi eror
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        // Cari Post berdasarkan ID
        $post = Post::findOrFail($id);

        if ($post) {
            $user = auth()->user();

            if ($post->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Post bukan milik ' . $user->username,
                ], 403);
            }

            // Memutakhirkan Post
            $post->update([
                'title' => $request->title,
                'description' => $request->description,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Post ' . $post->title . ' termutakhirkan',
                'data' => $post,
            ], 200);
        }

        // Data Post tak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Post tak ditemukan',
        ], 404);
    }

    public function destroy($id)
    {
        // Cari Post berdasarkan ID
        $post = Post::findOrFail($id);

        if ($post) {
            $user = auth()->user();

            if ($post->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Post bukan milik ' . $user->username,
                ], 403);
            }

            // Hapus Post
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post terhapus',
            ], 200);
        }

        // Data Post tak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Post tak ditemukan',
        ], 404);
    }
}
