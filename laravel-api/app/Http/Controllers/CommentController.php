<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Events\CommentStored;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Lis data semua Comment',
            'data' => $comments,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required',
            'post_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comment = Comment::create([
            'content' => $request->content,
            'post_id' => $request->post_id,
        ]);

        event(new CommentStored($comment));

        if ($comment) {
            return response()->json([
                'success' => true,
                'message' => 'Comment berhasil dibuat',
                'data' => $comment,
            ], 201);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment gagal tersimpan',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Comment::findOrFail($id);

        return response()->json([
            'success' => true,
            'message' => 'Detail sebuah Comment',
            'data' => $comment,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comment = Comment::findOrFail($id);

        if ($comment) {
            $user = auth()->user();

            if ($comment->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Comment bukan milik ' . $user->username,
                ], 403);
            }

            $comment->update([
                'content' => $request->content,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment termutakhirkan',
                'data' => $comment,
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment tak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);

        if ($comment) {
            $user = auth()->user();

            if ($comment->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Comment bukan milik ' . $user->username,
                ], 403);
            }

            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment terhapus',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment tak ditemukan',
        ], 404);
    }
}
