<?php

namespace App\Listeners;

use App\Events\CommentStored;
use App\Mail\CommentAuthorMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailToCommentAuthor implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CommentStored  $event
     * @return void
     */
    public function handle(CommentStored $event)
    {
        Mail::to($event->comment->user->email)->send(new CommentAuthorMail($event->comment));
    }
}
