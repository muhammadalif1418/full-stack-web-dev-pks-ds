<?php

namespace App\Listeners;

use App\Events\OtpCodeTeregenerasiEvent;
use App\Mail\OtpCodeTeregenerasiMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class KirimOtpCodeRegenerasi implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpCodeTeregenerasiEvent  $event
     * @return void
     */
    public function handle(OtpCodeTeregenerasiEvent $event)
    {
        Mail::to($event->otpCode->user->email)->send(new OtpCodeTeregenerasiMail($event->otpCode));
    }
}
