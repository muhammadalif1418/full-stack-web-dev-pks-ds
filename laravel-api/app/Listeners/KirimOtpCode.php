<?php

namespace App\Listeners;

use App\Events\UserTerdaftarEvent;
use App\Mail\UserTerdaftarMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class KirimOtpCode implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserTerdaftarEvent  $event
     * @return void
     */
    public function handle(UserTerdaftarEvent $event)
    {
        Mail::to($event->otpCode->user->email)->send(new UserTerdaftarMail($event->otpCode));
    }
}
