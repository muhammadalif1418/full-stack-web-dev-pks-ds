<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>

  <style>
    body {
      font: 14px sans-serif;
    }
  </style>
</head>
<body>
  <p>Saudara <b>{{ $comment->post->user->name }}</b> ({{ '@' . $comment->post->user->username }}),</p>
  <p>Post Anda yang berjudul <b>{{ $comment->post->title }}</b> telah dikomentari oleh <b>{{ $comment->user->name }}</b> ({{ '@' . $comment->user->username }}):</p>
  <blockquote><b>{{ $comment->content }}</b></blockquote>
</body>
</html>
