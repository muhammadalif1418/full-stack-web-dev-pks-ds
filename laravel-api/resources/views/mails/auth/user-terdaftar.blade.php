<!DOCTYPE html>
<html lang="id">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>User Terdaftar @ {{ config('app.name') }}</title>

  <style>
    body {
      font: 14px sans-serif;
    }
  </style>
</head>
<body>
  <p>Hai, <b>{{ $otpCode->user->name }}</b> ({{ '@' . $otpCode->user->username }})</p>
  <p>Selamat datang di <b>{{ config('app.name') }}</b></p>
  <p>Ini adalah kode OTP Anda: <b>{{ $otpCode->otp }}</b></p>
  <p>Kode OTP ini berlaku <b>5 menit</b></p>
  <p><i>Jangan berikan kode ini kepada siapa pun~</i></p>
</body>
</html>
