// 🔥 SOAL 1 🔥

// Luas 💳
const lPp = (p, l) => p * l

// Keliling 💳
const kPp = (p, l) => 2 * (p + l)

// 💨
console.log(lPp(6, 4)) // 24
console.log(kPp(6, 4)) // 20

// 🔥 SOAL 2 🔥

const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => console.log(`${firstName} ${lastName}`),
  }
}

// Driver code
newFunction('William', 'Imoh').fullName() // William Imoh

// 🔥 SOAL 3 🔥

const newObject = {
  firstName: 'Muhammad',
  lastName: 'Iqbal Mubarok',
  address: 'Jalan Ranamanyar',
  hobby: 'playing football',
}

const {firstName, lastName, address, hobby} = newObject

// Driver code
console.log(firstName, lastName, address, hobby) // Muhammad Iqbal Mubarok Jalan Ranamanyar playing football

// 🔥 SOAL 4 🔥

const west = ['Will', 'Chris', 'Sam', 'Holly']
const east = ['Gill', 'Brian', 'Noel', 'Maggie']

const combined = [...west, ...east]

// Driver code
console.log(combined)
// [
//   'Will', 'Chris',
//   'Sam',  'Holly',
//   'Gill', 'Brian',
//   'Noel', 'Maggie'
// ]

// 🔥 SOAL 5 🔥

const planet = 'earth'
const view = 'glass'

var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`

// 💨
console.log(before) // Lorem glass dolor sit amet, consectetur adipiscing elit, earth
